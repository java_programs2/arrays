package practiceprograms;

public class Array_frequency {

	public static void main(String[] args) {
		int[] a = { 10, 20, 30, 40, 20, 10, 10, 40, 50 };
		int size=a.length;
		int freq[]=new int[size];
		
		for(int i=0; i<size; i++)
		{
			int count=1;
			for(int j=i+1; j<size; j++)
			{
				if(a[i]==a[j])
				{
					count++;
					freq[j]=-1;
				}
			}
			if(freq[i]!=-1)
			{
			freq[i]=count;
			}
		}
		
		for(int i=0;i<size;i++)
		{
		if(freq[i]!=-1)
		{
		System.out.println(a[i]+" "+"occurs"+" "+freq[i]+" "+"times");
		}
		}
	}

}
