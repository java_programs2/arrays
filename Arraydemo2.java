package practiceprograms;

public class Arraydemo2 {

	/*
	static void array(int[]a) {
		for(int i:a) {
			System.out.println(i);
		}
	}
	public static void main(String[] args) {
		
int[]a= {1,2,3,4,5};
array(a);
	}
*/
	static int[] array(){
		
		return new int[] {10,20,30,40};
	}
	
	
	public static void main(String[] args) {
		
		int[]a= array();
		for(int i=0;i<a.length;i++)  
			System.out.println(a[i]);
	}	
	
}
