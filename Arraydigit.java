package practiceprograms;

public class Arraydigit {

	
	public static void main(String[] args) {
		int arr[]= {12,356,3,6,1234};
		
	Arraydigit obj=new Arraydigit();
	
	
	int result=obj.find_even_digits(arr);
	System.out.println(result);
	
	}
	

	private int find_even_digits(int arr[]) {
		int result=0;
		for(int i=0;i<arr.length;i++) {
		if(	find_digits(arr[i])) {
			result++;
			System.out.print(arr[i]+" ");
		}
		}
		return result;
	}
	
	public boolean find_digits(int num){
		int digit=0;
		while(num>0) {
			num=num/10;
			digit++;
		}
		return digit%2==0;
	}
}
