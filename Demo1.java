package practiceprograms;

public class Demo1 implements Interface_Demo, Interface_demo2{

	public void add(int a, int b) {
		System.out.println(a+b);
	}
	public void print() {
		System.out.println("achieving multiple inheritance using interface");
	}
	public static void main(String[] args) {
		Demo1 obj= new Demo1();
		obj.add(3, 4);
		obj.sub(8, 3);
		Interface_Demo.multiply(4,4);
		obj.print();
	}

}
